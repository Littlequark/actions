//
//  ACNetworkClient.swift
//  ACTIONS
//
//  Created by tstepanov on 06.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage


class ACNetworkClient: NSObject {
    
    private var httpHeaders:HTTPHeaders = ["Cache-Control":"no-cache",
                                           "X-Accept-Version":"v5"]
    
    func requestApiTokenWithCompletion(_ completion:@escaping (_ token:String?)->Void) {
        let URLstring = "https://tsenoanalizator.ru/api/mobile/get_app_token?device_type=test&os_type=android&os_version=test&client_version=test&lat=60.10&lng=30.33"
        let requestURL = URL(string: URLstring)
        
        Alamofire.request(requestURL!,
                          method: .post,
                          headers: httpHeaders)
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Error while fetching API token : \(response.result.error?.localizedDescription ?? "")")
                    completion(nil)
                    return
                }
                
                guard let value = response.result.value as? [String: Any]
                    else {
                        completion(nil)
                        return
                }
                
                print("Response: \(value)")
                guard let tokenDict  = value["response"] as? [String:String]
                    else {
                        completion(nil)
                        return
                }
                
                guard let token = tokenDict["token"]
                    else {
                        completion(nil)
                        return
                }
                
                self.httpHeaders["X-Client-Token"] = token;
                completion(token)
        }
    }
    
    func requestCashbackListWithCompletion(_ completion:@escaping (_ cashbacks:[ACCashback]?)->Void) {
        let URLstring = "https://tsenoanalizator.ru/api/mobile/rebate/index?lat=60.10&lng=30.33"
        let requestURL = URL(string: URLstring)
        
        Alamofire.request(requestURL!,
                          headers: httpHeaders)
            .responseData(completionHandler: { (response) -> Void in
                
                guard response.result.isSuccess else {
                    print("Error while fatching cashback : \(response.result.error?.localizedDescription ?? "")")
                    completion(nil)
                    return
                }
                
                guard let cashbacks = self.parseCashBacksData(responseData:response.result.value!)
                    else {
                        completion(nil)
                        return
                }
                completion(cashbacks)
        })
    }
    
    private func parseCashBacksData(responseData:Data!) -> [ACCashback]? {
        var response:Response?
        do {
            let decoder = JSONDecoder()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "YYYY-MM-dd"
            decoder.dateDecodingStrategy = .formatted(dateFormatter)
            response = try decoder.decode(Response.self, from: responseData!) }
        catch DecodingError.keyNotFound(let key, let context) {
            print("Missing key: \(key)")
            print("Debug description: \(context.debugDescription)")
        }
        catch DecodingError.valueNotFound(let type, let context) {
            print("Missing value: \(type)")
            print("Debug description: \(context.debugDescription)")
        }
        catch DecodingError.typeMismatch(let key, let context) {
            print("Type mismatch: \(key)")
            print("Debug description: \(context.debugDescription)")
        }
        catch {
            print("Unknown error")
        }
        return response?.response
    }
    
    func loadImageWithUrl(_ urlString:String?, completion:@escaping(_ image:UIImage?)->Void) {
        
        guard urlString != nil , let requestURL = URL(string:urlString!) else {
            completion(nil)
            return
        }
        
        Alamofire.request(requestURL, headers:httpHeaders).responseImage{ (responseImage) in
            
            guard responseImage.result.isSuccess else {
                print("Error while loading image : \(responseImage.result.error?.localizedDescription ?? "")")
                completion(nil)
                return
            }
            
            guard let image = responseImage as? UIImage else {
                completion(nil)
                return
            }
            completion(image)
        }
    }
    
}
