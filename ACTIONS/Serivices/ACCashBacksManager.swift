//
//  ACCashBacksManager.swift
//  ACTIONS
//
//  Created by tstepanov on 08.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import Foundation

protocol ACCashbackManagerDelegate {
    func cashbackManagerDidReloadData(_ cashbackManager:ACCashbacksManager)
}

class ACCashbacksManager {
    
    var delegate:ACCashbackManagerDelegate?
    
    // Instead of using (DI) dependency injection 
    private let networkClient = ACNetworkClient()
    private var cashbacks:[ACCashback]?
    
    //MARK: - Public methods
    
    func loadCashBacks() {
        networkClient.requestApiTokenWithCompletion { (token:String?) in
            self.networkClient.requestCashbackListWithCompletion({ (fetchedCashbacks:[ACCashback]?) in
                self.cashbacks = fetchedCashbacks;
                self.delegate?.cashbackManagerDidReloadData(self)
            })
        }
    }
    
    func nuberOfItems()->Int {
        guard cashbacks != nil else {
            return 0
        }
        return cashbacks!.count
    }
    
    func item(at index:Int) -> ACCashback? {
        guard index < cashbacks!.count else {
            return nil
        }
        return cashbacks![index]
    }
    
}
