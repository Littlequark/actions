//
//  Response.swift
//  ACTIONS
//
//  Created by tstepanov on 08.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import Foundation

struct Response :  Codable {
    var status : String
    var response : [ACCashback]
}
