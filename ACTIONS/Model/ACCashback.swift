//
//  ACCashback.swift
//  ACTIONS
//
//  Created by tstepanov on 06.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import Foundation

struct ACCashback : Codable {
    
    var conditions:String
    var description:String
    var start:Date
    var end:Date
    var fastCheckout:Int
    var cashbackId:String
    var marketBrandId:String
    var status:String
    var type:String
    // product part
    var productId:String
    var productImageURL:String
    var productPrice:Double
    var productName:String
    // rebate part
    var rebateCost: Int
    var rebateCostMax: Int
    var rebateCostTogether: Int
    
    private enum CodingKeys: String, CodingKey {
        case conditions
        case description
        case start
        case end
        case fastCheckout = "fast_checkout"
        case cashbackId = "id"
        case marketBrandId = "market_brand_id"
        case status
        case type
        case productId = "product_id"
        case productImageURL = "product_image"
        case productPrice = "product_price"
        case productName = "product_name"
        case rebateCost = "rebate_cost"
        case rebateCostMax = "rebate_cost_max"
        case rebateCostTogether = "rebate_cost_together"
    }

}
