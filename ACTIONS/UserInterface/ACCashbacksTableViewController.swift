//
//  ACCashbacksTableViewController.swift
//  ACTIONS
//
//  Created by tstepanov on 06.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import UIKit

class ACCashbacksTableViewController: UITableViewController, ACCashbackManagerDelegate {

    private var cashbacks:[ACCashback]?
    
    private let cashbacksManager = ACCashbacksManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsetsMake(16, 0, 16, 0)
        cashbacksManager.delegate = self
        cashbacksManager.loadCashBacks()
        self.clearsSelectionOnViewWillAppear = true
    }
    
    // MARK: - ACCashbackManagerDelegate

    func cashbackManagerDidReloadData(_ cashbackManager: ACCashbacksManager) {
        tableView.reloadData()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cashbacksManager.nuberOfItems()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CashbackCellReuseId", for: indexPath) as! ACCashbackTableViewCell
        guard let cashback = cashbacksManager.item(at:indexPath.row) else {
            return UITableViewCell()
        }
        cell.cashbackObject = cashback;
        return cell
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let selectedIndexPath = tableView.indexPathForSelectedRow else {
            return
        }
        guard let cashbackDetailViewController = segue.destination as? ACCCashbackDetailViewController else {
            return
        }
        guard let selectedCashback = cashbacksManager.item(at:selectedIndexPath.row) else {
            return
        }
        cashbackDetailViewController.cashback = selectedCashback
    }
 
}
