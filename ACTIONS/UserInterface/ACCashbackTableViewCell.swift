//
//  ACCashbackTableViewCell.swift
//  ACTIONS
//
//  Created by tstepanov on 06.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import UIKit


class ACCashbackTableViewCell: UITableViewCell {

    @IBOutlet var cashbackImageView:UIImageView!
    @IBOutlet var productNameLabel:UILabel!
    @IBOutlet var maxCashbackLabel:UILabel!
    
    var cashbackObject:ACCashback? {
        didSet {
            self.cashbackImageView.af_setImage(withURL:URL(string:cashbackObject!.productImageURL)!)
            self.productNameLabel.text = firstWord(of:cashbackObject!.productName).uppercased()
            self.maxCashbackLabel.text = "Кэшбек: \(cashbackObject!.rebateCost) (макс:\(cashbackObject!.rebateCostMax))"
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.cashbackImageView.af_cancelImageRequest()
        self.cashbackImageView.image = nil
    }

    private func firstWord(of string:String) -> String {
        let whitespaceIndex = string.index(of:" ")!
        let firstWord = "\(string[...whitespaceIndex])"
        return firstWord
    }
    
}
