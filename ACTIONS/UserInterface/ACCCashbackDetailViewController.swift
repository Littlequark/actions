//
//  ACCCashbackDetailViewController.swift
//  ACTIONS
//
//  Created by tstepanov on 08.01.2018.
//  Copyright © 2018 persTim. All rights reserved.
//

import UIKit

class ACCCashbackDetailViewController: UIViewController {

    var cashback:ACCashback? {
        didSet {
        
        }
    }
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var periodLabel:UILabel!
    @IBOutlet var descriptionLabel:UILabel!
    @IBOutlet var baseCashbackLabel:UILabel!
    @IBOutlet var maxCashbackLabel:UILabel!
    @IBOutlet var priceLabel:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    //MARK: - Private methods
    
    private func setupView() {
        self.productImageView.af_setImage(withURL:URL(string:cashback!.productImageURL)!)
        self.nameLabel.text = cashback?.productName
        self.title = firstWord(of:cashback!.productName)
        self.descriptionLabel.text = cashback?.conditions
    }
    
    private func firstWord(of string:String) -> String {
        let whitespaceIndex = string.index(of:" ")!
        let firstWord = "\(string[...whitespaceIndex])"
        return firstWord
    }
    
}
